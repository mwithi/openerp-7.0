# OpenERP v7.0 #

### What is this repository for? ###

* Custom OpenERP v7.0 used for ISF/CUAMM in Tosamaganga Hospital (Tanzania) & Wolisso in St. Luke Hospital (Ethiopia)

### How do I get set up? ###

#### Requirements ####

* Python 2.7.8
* Python libraries in requirements.txt
* PostgreSQL Server

#### Istructions ####

* clone branch openeerp7_tosa
* setup openerp-server.conf
* launch openerp-server.py

### Contribution guidelines ###

* Fork, develop & pull-request
* Double analyze the existing code, don't write what is already written, be DRY (Don't Repeat Yourself)
* Dump code is better than clever one when is time to share
* Write comments when only "YOU" know what you are doing
* Optimize only after achieved
* Less is more!

### Who do I talk to? ###

* Repo owner or admin: [mwithi](https://github.com/mwithi)
* Other community or team contact: [ISF - Informatici Senza Frontiere ONLUS](http://www.informaticisenzafrontiere.org/)